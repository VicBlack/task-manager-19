package ru.t1.kupriyanov.tm.api.repository;

import ru.t1.kupriyanov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name, String description);

}
