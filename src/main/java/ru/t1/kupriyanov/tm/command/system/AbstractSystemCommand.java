package ru.t1.kupriyanov.tm.command.system;

import ru.t1.kupriyanov.tm.api.service.ICommandService;
import ru.t1.kupriyanov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
