package ru.t1.kupriyanov.tm.exception.entity;

public final class ProjectNotFoundException extends AbstractEntityException {

    public ProjectNotFoundException() {
        super("Error! Project wasn't found...");
    }

}
