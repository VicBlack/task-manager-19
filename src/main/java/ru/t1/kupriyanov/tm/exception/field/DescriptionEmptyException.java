package ru.t1.kupriyanov.tm.exception.field;

public final class DescriptionEmptyException extends AbstractFiledException {

    public DescriptionEmptyException() {
        super("Error! Project description is empty...");
    }

}
