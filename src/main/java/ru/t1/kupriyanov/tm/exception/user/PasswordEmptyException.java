package ru.t1.kupriyanov.tm.exception.user;

public class PasswordEmptyException extends AbstractUserException {

    public PasswordEmptyException() {
        super("Error! Password is empty!");
    }

}
